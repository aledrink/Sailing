function move(obj,attr,target,speed,callback){
                
    clearInterval(obj.timer); 
    //获取元素目前的位置
    var current=parseInt(getStyle(obj,attr));
    //判断速度的正负
    //如果使从0向800移动，则speed为正
    //如果从800向0移动，则speed为负
    if(current>target){
        //此时速度应为负值
        speed=-speed;
    } 

    // alert(oldValue);
    //向执行动画的对象中添加一个timer属性,用来博爱村它自己的定时器的标识
     obj.timer=setInterval(function(){
    //获取box原来的Left值
     var oldValue=parseInt(getStyle(obj,attr));
     //在旧值的基础上增加
     var newValue=oldValue+speed;
     //判断newValue是否小于0
      //向左移动的时候，需要判断newValue是否小于target
     //向右移动的时候，需要判断newValue是否大于target
     if((speed<0&&newValue<target)||(speed>0&&newValue>target)){
         newValue=target;
     }
     //将新值设置给box1
     obj.style[attr]=newValue+"px";
     //当元素移动到0px时，让其停止执行动画
    

     if(newValue==target){
        //达到目标，关闭定时器
        clearInterval(obj.timer);
        //动画执行完毕,调用回调函数
        callback&&callback();
     }

       
    },30)
};
function getStyle(obj,name){
      if(window.getComputedStyle){
        //正常浏览器的方式
        return getComputedStyle(obj,null)[name];
      }
      else{
         //IE8的方式
         return obj.currentStyle[name];
      }
      
     
  }